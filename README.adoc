= terraform on Apple silicon
:hardbreaks-option:

*HOW TO* use Terraform without any problems *ON* MacOS with Apple silicon:

==== install https://github.com/tfutils/tfenv[tfenv]:
```
brew install tfenv
```

==== add to your `~/.zshrc`:
```
export TFENV_ARCH=amd64
export GODEBUG=asyncpreemptoff=1
```
_note:_
first is needed for using terraform via Rosetta - it's slower, but works.
last is needed for https://yaleman.org/post/2021/2021-01-01-apple-m1-terraform-and-golang/[fix] terraform plan/apply errors.

==== restart you terminal.

==== try to install some terraform version:
```
tfenv install 1.3.7
```
_note:_
if you get 403 error, try to turn off your VPN.

_note:_
by default, tfenv install and switch to another terraform version automatically, on-the-fly, when you change work dir. So, you don't need do anything more, just use terraform from any dir:  

==== if you are working not alone you also need to do:
```
terraform providers lock -platform=linux_amd64
```

==== install and use linter:
```
brew install tflint
```
```
tflint
```
_note:_
just execute it in your terraform files dir.
